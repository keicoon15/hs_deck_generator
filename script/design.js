function load() {
    try {
        loadCard()
        loadClassCardList()
        loadFilter()
        console.log("load succ")
        //@event
        let input = document.getElementById("filterInput")
        input.addEventListener('keydown', e => {
            if(e.key !== 'Enter') return
            const query = input.value
            
            let ul = document.getElementById("cardListUl")
            if(query === "") {
                for(let index = 0, len = ul.childNodes.length; index < len; index++)
                    ul.childNodes[index].style.visibility = "visible"
            } else {
                for(let index = 0, len = ul.childNodes.length; index < len; index++) {
                    if(!_.includes(ul.childNodes[index].textContent, query))
                        ul.childNodes[index].style.visibility = "hidden"
                    else
                        ul.childNodes[index].style.visibility = "visible"
                }
            }
            filterCardList()
        })
    } catch(e) {
        console.error("load cardData failed: ", e)
    }
}

let CardJSON = null
let CardFilterByClass = new Map()
function loadCard(callType, opt) {
    $.getJSON("https://api.hearthstonejson.com/v1/latest/koKR/cards.collectible.json", {
        format: "json"
    })
        .done(function (data) {
            CardJSON = data
        });
}
const ClassList = {
    data: {
        en: ["", "NEUTRAL", "MAGE", "SHAMAN", "WARRIOR", "ROGUE", "HUNTER", "PALADIN", "PRIEST", "WARLOCK", "DRUID"],
        ko: ["", "중립", "마법사", "주술사", "전사", "도적", "사냥꾼", "성기사", "사제", "흑마법사", "드루이드"],
        color: ["", "DarkGray", "DodgerBlue", "MediumBlue", "DeepPink", "MidnightBlue", "OliveDrab", "GoldenRod", "Pink", "DarkOrchid", "ForestGreen" ]
    },
    GetList: (lang) => {
        return ClassList.data[lang]
    },
    GetConvertLang: (data, prev, aftr) => {
        const classlist_prev = ClassList.GetList(prev)
        const classlist_aftr = ClassList.GetList(aftr)
        const index = _.findIndex(classlist_prev, c => c === data)
        return classlist_aftr[index]
    },
    GetColor: (data) => {
        const index = _.findIndex(ClassList.GetList('en'), c => c === data)
        return ClassList.data.color[index]
    },
}
const filterList = {
    '': 'cost',
    '카드 종류': 'type',
    '등급': 'rarity',
    '비용': 'cost',
    '공격력': 'attack',
    '체력': 'health'
}
let prevSelectedLi = null
function loadClassCardList() {
    let classSelect = document.getElementById("classSelect")
    const _classList = ClassList.GetList('ko')
    _.forEach(_classList, t => {
        let opt = document.createElement("option")
        opt.text = t
        classSelect.appendChild(opt)
    })
    classSelect.addEventListener('change', () => {
        const selectedClass = document.getElementById("classSelect").value
        let ul = document.getElementById("cardListUl")
        ul.innerHTML = ""
        if(selectedClass === "") return

        const cardList = loadCardList('clsss', selectedClass)
        _.each(cardList, card => {
            let li = document.createElement("li")
            li._card = card
            updateColorLi(li, card.playerClass)
            li.appendChild(document.createTextNode(card.name))
            ul.appendChild(li)
            li.addEventListener('click', (e) => {
                udpateCardLi(li)
                updateCardInfo(card)
            })
            li.addEventListener('contextmenu', (e) => {
                deckManager.Add(card)
                window.event.returnValue = false
            })
        })
        deckManager.MyClass = ClassList.GetConvertLang(selectedClass, 'ko', 'en')
    })
}

function loadCardList(callType, cardOpt) {
    if(CardJSON === null) return
    let fetcher = {
        "clsss": () => {
            const lang = cardOpt
            const className_ko = cardOpt
            const className_en = ClassList.GetConvertLang(lang, 'ko', 'en')
            if(CardFilterByClass.has(className_en)) return CardFilterByClass.get(className_en)
            const cardList = _(CardJSON)
                .filter(card => card.type !== 'HERO')
                .filter(card => card.playerClass === className_en)
                .sortBy(card => card.cost)
                .value()
            CardFilterByClass.set(className_en, cardList)
            return cardList
        }
    }[callType]
    return fetcher()
}

function loadFilter() {
    let filterSelect = document.getElementById("filterSelect")
    _.each(filterList, (value, key) => {
        let opt = document.createElement("option")
        opt.text = key
        filterSelect.appendChild(opt)
    })
    filterSelect.addEventListener('change', () => {
        const selectedFilter = document.getElementById("filterSelect").value
        filterCardList(selectedFilter)
    })
}

function filterCardList(sortType = '') {
    let ul = document.getElementById("cardListUl")
    const len = ul.childNodes.length
    if(len < 1) return
    
    const type = filterList[sortType]
    const childs = _.sortBy(ul.childNodes, li => li.style.visibility === 'hidden' ? Number.MAX_VALUE : li._card[type])
    ul.innerHTML = ""
    _.each(childs, c => {
        ul.appendChild(c)
    })
}

function updateMyDeckUI(card, cardNum) {
    let ul = document.getElementById("deckListUl")
    let index = 0, len = ul.childNodes.length
    for(; index < len && (ul.childNodes[index].innerText !== card.name && ul.childNodes[index].innerText !== `${card.name} x2`); index++) ;
    if(cardNum > 1) {
        ul.childNodes[index].textContent = `${card.name} x2`
    }
    else if(cardNum > 0) {
        if(index < len) {
            ul.childNodes[index].textContent = card.name
        }
        else {
            let li = document.createElement("li")
            li._card = card
            updateColorLi(li, card.playerClass)
            li.appendChild(document.createTextNode(card.name))
            ul.appendChild(li)
            li.addEventListener('click', (e) => {
                udpateCardLi(li)
                updateCardInfo(card)
            })
            li.addEventListener('contextmenu', (e) => {
                deckManager.Delete(card)
                window.event.returnValue = false
            })
        }
    }
    else {
        ul.removeChild(ul.childNodes[index])
    }
    let p = document.getElementById("myCardNum")
    p.innerText = `현재 카드 수 : ${deckManager.MyCardNum}`
}

function updateMyClass() {
    let p = document.getElementById("myClass")
    p.innerText = `직업 : ${ClassList.GetConvertLang(deckManager.MyClass, 'en', 'ko')}`
}

function wrapText(key, value) {
    return `<p class="customfontP">${key}</p>${value}`
}

function updateColorLi(li, className) {
    li.style.color = ClassList.GetColor(className)
}
function udpateCardLi(li) {
    if (prevSelectedLi) {
        prevSelectedLi.style.fontWeight = 'normal'
        prevSelectedLi.style.textDecoration = 'none'
        prevSelectedLi.style.fontSize = '100%'
    }
    li.style.fontWeight = 'bold'
    li.style.textDecoration = 'underline'
    li.style.fontSize = '120%'
    prevSelectedLi = li
}
function updateCardInfo(card) {
    $("html, body").animate({ scrollTop: 0 }, "fast")
    const div = document.getElementById("cardInfoDiv")
    div.innerHTML = ""
    _.each(card, (value ,key) => {
        let content = {
            type: wrapText('카드 종류', card.type),
            rarity: wrapText('등급', card.rarity),
            name: wrapText('이름', card.name),
            cost: wrapText('비용', card.cost),
            attack: wrapText('공격력', card.attack),
            health: wrapText('체력', card.health),
            flavor: wrapText('flavor', card.flavor),
            text: wrapText('설명', card.text)
        }[key]
        if(content)
            div.innerHTML += content
    })
}