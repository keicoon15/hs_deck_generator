/*
Assuming jQuery Ajax instead of vanilla XHR
https://gist.github.com/techslides/9569cb7c7caa5e95bb7b#file-github-gist-api-js
*/

//Get Github Authorization Token with proper scope, print to console
class github_gist_api {
    static authorizations(USERNAME, PASSWORD) {
        $.ajax({
            url: 'https://api.github.com/authorizations',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(`${USERNAME}:${PASSWORD}`));
            },
            data: '{"scopes":["gist"],"note":"ajax gist test for a user"}'
        }).done(function (response) {
            console.log(response.token)
        });
    }
    //Create a Gist with token from above
    static create(token, content) {
        $.ajax({
            url: 'https://api.github.com/gists',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", token);
            },
            data: '{"description": "a gist for a user with token api call via ajax","public": true,"files": {"file1.txt": {"content": "String file contents via ajax"}}}'        
        }).done(function (response) {
            console.log(response);
        });
    }
    //Using Gist ID from the response above, we edit the Gist with Ajax PATCH request
    static response(token, gistId) {
        $.ajax({
            url: `https://api.github.com/gists/${gistId}`,
            type: 'PATCH',
            // beforeSend: function (xhr) {
            //     xhr.setRequestHeader("Authorization", token);
            // },
            data: '{"description": "updated gist via ajax","public": true,"files": {"file1.txt": {"content": "updated String file contents via ajax"}}}'
        }).done(function (response) {
            console.log(response);
        });
    }
    static t() {
        $.ajax({
            url: 'https://api.github.com/gists/keicoon',
            type: 'GET',
            dataType: 'jsonp'
        }).success(function (gistdata) {
            console.log(gistdata)
        }).error(function (e) {
            // ajax error
        });
    }
}