window.onload = () => {
    console.log("logic call")
    ///TODO: design.js call
    load()
    ///@TODO: logic
    deckManager = new C_deckManager()
}
let deckManager = null 
class C_deckManager {
    constructor() {
        this.deckList = new Map()
        this.currentCardNum = 0

        this.class = ""
        this.maximumCardNum = 30
        this.autho = null
    }
    get MyCardNum() {
        return this.currentCardNum
    }
    get MyClass() {
        return this.class
    }
    set MyClass(className) {
        if(className !== 'NEUTRAL' && (this.currentCardNum < 1 || (() => {
            let len = 0
            this.deckList.forEach(card => card.card.playerClass !== 'NEUTRAL' && (++len))
            return len < 1
        })())) {
            this.class = className
            updateMyClass()
        } else if (this.currentCardNum < 1 && className === 'NEUTRAL') {
            this.ResetClass()
        }
    }
    Reset() {
        if(this.currentCardNum < 1) return

        this.ResetClass()
        this.currentCardNum = 0
        this.deckList.forEach(card => updateMyDeckUI(card.card, 0))
        this.deckList.clear()
    }
    ResetClass() {
        this.class = ""
        updateMyClass()
    }
    IsAddValid(card) {
        if(card.playerClass !== 'NEUTRAL' && this.MyClass !== "" && this.MyClass !== card.playerClass) return false
        if(this.currentCardNum >= this.maximumCardNum) return false
        
        const cardId = card.id
        const isNotFirstCard = this.deckList.has(cardId)
        if(isNotFirstCard) {
            const cardInfo = this.deckList.get(cardId)
            const cardRarity = card.rarity
            if(cardRarity === 'LEGENDARY') return false
            else if(cardInfo.num >= 2) return false
        }
        return true
    }
    Add(card) {
        this.MyClass = card.playerClass
        
        if(this.IsAddValid(card)) {
            ++this.currentCardNum
            const cardId = card.id
            const isFirstCard = !this.deckList.has(cardId)
            if(isFirstCard) {
                this.deckList.set(cardId, {card, num:1})
                updateMyDeckUI(card, 1)    
            }
            else {
                ++this.deckList.get(cardId).num
                updateMyDeckUI(card, 2)
            }
        }
    }
    IsDeleteValid(card) {
        const cardId = card.id
        if(this.deckList.has(cardId)) return true
        return false
    }
    Delete(card) {
        if(this.IsDeleteValid(card)) {
            --this.currentCardNum
            const cardId = card.id
            const cardInfo = this.deckList.get(cardId)
            if(cardInfo.num > 1) {
                --this.deckList.get(cardId).num
                updateMyDeckUI(card, 1)
            }
            else {
                this.deckList.delete(cardId)
                updateMyDeckUI(card, 0)
            }
            if(this.currentCardNum < 1)
                this.ResetClass()
        }
    }
    //
    GetToken(aftrFunc) {
        if(!this.autho) {
            let USERNAME = 'keicoon'//prompt("Input yuor github USERNAME")
            let PASSWORD = 'llen4636'//prompt("Input yuor github PASSWORD")
            github_gist_api.authorizations(USERNAME, PASSWORD)
        } //else aftrFunc()
    }
    Save() {
        // this.GetToken()
        // // this.GetToken(()=>{
        // //     github_gist_api.create(this.autho.token)
        // // })
        github_gist_api.response()
    }
    Load() {
       this.GetToken(()=>{
            github_gist_api.response(this.autho.token)
        }) 
    }
}